﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Windows;
using System.Security;
using System.Security.AccessControl;

using Rhino5x64;
using RhinoScript4;

using Rhino;

using Microsoft.Win32;

using System.Management.Automation.Runspaces;

namespace ProvingGround.FreighterUtils
{
    public class Executor
    {

        #region Private Members

        // Configuration data for executing launch
        private DocConfig _docConfig;
        private string _appDirectory;

        #endregion

        #region Constructors

        public Executor(string AppDirectory)
        {

            _appDirectory = AppDirectory;

        }

        /// <summary>
        /// Try to create DocConfig from freight.xml in target directory
        /// </summary>
        /// <returns></returns>
        public bool LoadConfig()
        {

            if (File.Exists(_appDirectory + "\\freight.xml"))
            { 
                try
                {
                    _docConfig = DocConfig.ReadConfig(_appDirectory + "\\freight.xml");
                    return true;
                }
                catch 
                {
                    return false;
                }
            }
            else
            {
                return false;
            }

        }

        #endregion

        #region Public Members

        /// <summary>
        /// Simple attribute lookup for executor class
        /// </summary>
        /// <param name="Lookup"></param>
        /// <returns></returns>
        public string Attribute(string Lookup)
        {

            return _docConfig.GetAttributeValue(Lookup);

        }

        /// <summary>
        /// Primary engine for launching freight
        /// </summary>
        public void LaunchFreight()
        {

            try
            {
                // prelimimary kill of any old C\ProgramData\ProvingGround instances that may be floating around
                try
                {
                    if (Directory.Exists(@"C:\ProgramData\ProvingGround"))
                    {
                        System.IO.Directory.Delete(@"C:\ProgramData\ProvingGround", true);
                    }
                }
                catch
                {

                }

                // First check for installation of utilities in the registry
                string m_valueName = "Freighter utilities";
                string m_plugInName = "FreighterLaunchUtils";

                InstallRhinoPlugIn(Resources.ProvingGround_FreighterLaunchUtils,
                        m_plugInName, m_valueName);

                // Then open rhino:

                // Get file information from the config file
                string m_rhinoFileName = _docConfig.GetAttributeValue("RhinoFileName");
                string m_ghFileName = _docConfig.GetAttributeValue("GrasshopperFileName");

                // Launch Rhino
                string m_rhinoprogramID = "Rhino5x64.Application";
                Type type = Type.GetTypeFromProgID(m_rhinoprogramID);
                dynamic rhino = Activator.CreateInstance(type);

                // Create instance of Rhino application
                IRhino5x64Application m_RhinoApp = rhino as IRhino5x64Application;
                m_RhinoApp.Visible = Convert.ToInt16(1);  // 0 = hidden,  1 = visible
                m_RhinoApp.ReleaseWithoutClosing = 1; // 0 = close with instance, 1 = release

                IRhinoScript m_RhinoScript = m_RhinoApp.GetScriptObject() as IRhinoScript;

                // Then load a rhino file if one is specified in the config file
                if (File.Exists(_appDirectory + "\\Files\\" + m_rhinoFileName))
                {
                    m_RhinoApp.RunScript("-_Open " + "\"" + _appDirectory + "\\Files\\" + m_rhinoFileName + "\"" + " _Enter", 0);
                }

                m_RhinoApp.BringToTop();

                // Then copy all plug-ins in the freight
                try
                {
                    DirectoryCopy(_appDirectory + "\\PlugIns", Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) 
                        + @"\Grasshopper\Libraries", true);
                }
                catch (Exception ex)
                {
                    System.Windows.Forms.MessageBox.
                        Show("Third party dependencies failed to copy. Error returned: " + ex.Message, "Freighter");
                }

                // Unblock all dependencies in the Grasshopper library folder
                try
                {
                    UnblockDependencies();
                }
                catch (Exception ex)
                {
                    System.Windows.Forms.MessageBox.
                        Show("There may be some blocked dependencies that could prevent Grasshopper from running properly. Error returned: " 
                        + ex.Message, "Freighter");
                }

                // Load grasshopper
                m_RhinoApp.RunScript("_Grasshopper", 1);

                // Expose rudimentary grasshopper automation commands
                var gh = rhino.GetPlugInObject("b45a29b1-4343-4035-989e-044e8580d9cf",
                        "00000000-0000-0000-0000-000000000000") as dynamic;
            
                // Open the target definition
                try
                {
                    gh.OpenDocument(_appDirectory + "\\Files\\" + m_ghFileName);
                }
                catch (Exception ex)
                {
                    System.Windows.Forms.MessageBox.
                        Show("Unable to load Grasshopper definition. Check the files directory and ensure that " + 
                        m_ghFileName + " is located there. Error returned: " + ex.Message, "Freighter");
                    throw;
                }

                // If there are panels that need to be updated (with attached references)
                if (_docConfig.AttachmentComps.Count > 0)
                {
                    gh.DisableSolver();

                    foreach (AttachmentComponent m_updateComponent in _docConfig.AttachmentComps)
                    {
                        
                        string m_instanceGuid = m_updateComponent.InstanceGuid.ToString();
                        string m_fileNames = "";

                        for (int i = 0; i < m_updateComponent.FileNames.Count; i++)
                        {
                            if (i > 0) m_fileNames += "n\"";
                            m_fileNames += m_updateComponent.FileNames[i];
                        }

                        // Runs the custom script from FreighterLaunchUtils
                        m_RhinoApp.RunScript("-_PgUpdateGrasshopperPanel " + "\"" + m_instanceGuid + "\" "
                                + "\"" + m_fileNames + "\"" + " _Enter", 0);
                        
                    }

                    gh.EnableSolver();
                }

                if (_docConfig.GetToggleValue("ShowGrasshopperRCP"))
                {
                    m_RhinoApp.RunScript("PgShowGrasshopperRCP", 1);
                }

                if (_docConfig.GetToggleValue("HideGrasshopper"))
                {
                    gh.HideEditor();
                }

            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show("Unable to launch freight. Error returned: " +
                    ex.Message);
               
                throw;
            }
        }

        #endregion

        #region Private Installer + File Management Methods

        /// <summary>
        /// Will unblock all files in the Grasshopper Libraries directory and its subdirectories
        /// </summary>
        /// <param name="directory"></param>
        private void UnblockDependencies()
        {

            // Requires the System.Management.Automation .dll loaded
            // from C:\Program Files (x86)\Reference Assemblies\Microsoft\WindowsPowerShell\3.0\
            // Invokes a Powershell command

            string scriptText = "Get-ChildItem -Path '" +
                Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) +
                "\\Grasshopper\\Libraries" + "' -Recurse | Unblock-File";

            Runspace m_runspace = RunspaceFactory.CreateRunspace();
            m_runspace.Open();

            Pipeline m_pipeline = m_runspace.CreatePipeline();
            m_pipeline.Commands.AddScript(scriptText);
            m_pipeline.Invoke();

            m_runspace.Close();

        }

        /// <summary>
        /// Install Rhino plug in from a byte array
        /// </summary>
        /// <param name="PlugInByteArray"></param>
        /// <param name="PlugInName"></param>
        /// <param name="KeyName"></param>
        private void InstallRhinoPlugIn(byte[] PlugInByteArray, string PlugInName, string ValueName)
        {

            try
            {

                // Path data
                //string m_plugInDirectory = @"C:\ProgramData\Proving Ground\Rhino\" + PlugInName;
                string m_plugInDirectory = @"C:\Proving Ground\Rhino\" + PlugInName;
                string m_plugInPath = m_plugInDirectory + "\\" + PlugInName + ".rhp";

                // Copy directory and plug-in
                if (!Directory.Exists(m_plugInDirectory)) Directory.CreateDirectory(m_plugInDirectory);
                System.IO.File.WriteAllBytes(m_plugInPath, PlugInByteArray);

                string m_keyNameCurrent =
                "HKEY_CURRENT_USER\\SOFTWARE\\McNeel\\Rhinoceros\\5.0x64\\Plug-ins";
                string m_keyNameLocal =
                    "HKEY_LOCAL_MACHINE\\SOFTWARE\\McNeel\\Rhinoceros\\5.0x64\\Plug-ins";

                // If it doesn't exist, then install utility plug-in
                if ((Registry.GetValue(m_keyNameCurrent, ValueName, null) == null) 
                    && (Registry.GetValue(m_keyNameLocal, ValueName, null) == null))
                {

                    bool m_setup = false;

                    //Registry Key Data
                    string m_rhkeypath = "SOFTWARE\\McNeel\\Rhinoceros\\5.0x64\\Plug-ins";
                    string m_rhguid = "0de7e423-7884-4130-b9ec-bff8e01b1561";

                    //User security to access registry
                    RegistrySecurity m_userSecurity = new RegistrySecurity();
                    RegistryAccessRule m_userRule = new RegistryAccessRule("Everyone", RegistryRights.FullControl, AccessControlType.Allow);
                    m_userSecurity.AddAccessRule(m_userRule);

                    /// First try current user path config
                    try
                    {
                        if (m_setup == false)
                        {
                            //Create registry key
                            RegistryKey m_rhreg = Registry.CurrentUser;
                            m_rhreg = m_rhreg.OpenSubKey(m_rhkeypath, true);
                            if (m_rhreg != null)
                            {
                                //Create plugin key
                                RegistryKey m_rhnewkey = m_rhreg.CreateSubKey(m_rhguid, RegistryKeyPermissionCheck.ReadWriteSubTree);
                                m_rhnewkey.SetValue("Name", ValueName);
                                m_rhnewkey.SetValue("FileName", m_plugInPath);
                                m_rhnewkey.Close();

                                m_setup = true;
                            }
                        }
                    }
                    catch { }

                    /// Try local machine config if current user path failed
                    try
                    {
                        if (m_setup == false)
                        {
                            //Create registry key
                            RegistryKey m_rhreg = Registry.LocalMachine;
                            m_rhreg = m_rhreg.OpenSubKey(m_rhkeypath, true);
                            if (m_rhreg != null)
                            {
                                // Create plugin key
                                RegistryKey m_rhnewkey = m_rhreg.CreateSubKey(m_rhguid, RegistryKeyPermissionCheck.ReadWriteSubTree);
                                m_rhnewkey.SetValue("Name", ValueName);
                                m_rhnewkey.SetValue("FileName", m_plugInPath);
                                m_rhnewkey.Close();

                                m_setup = true;
                            }
                        }
                    }
                    catch { }
                }
            }
            catch { System.Windows.Forms.MessageBox.Show("Rhino plug-in " + PlugInName + " failed to install."); }
            
        }

        /// <summary>
        /// Copy the contents of one directory into another (from MSDN)
        /// </summary>
        /// <param name="sourceDirectory"></param>
        /// <param name="targetDirectory"></param>
        /// <param name="copySubDirs"></param>
        private void DirectoryCopy(string sourceDirectory, string targetDirectory, bool copySubDirs)
        {
            // Get the subdirectories for the specified directory.
            System.IO.DirectoryInfo dir = new System.IO.DirectoryInfo(sourceDirectory);

            if (!dir.Exists)
            {
                throw new System.IO.DirectoryNotFoundException(
                    "Source directory does not exist or could not be found: "
                    + sourceDirectory);
            }

            System.IO.DirectoryInfo[] dirs = dir.GetDirectories();
            // If the destination directory doesn't exist, create it.
            if (!System.IO.Directory.Exists(targetDirectory))
            {
                System.IO.Directory.CreateDirectory(targetDirectory);
            }

            // Get the files in the directory and copy them to the new location.
            System.IO.FileInfo[] files = dir.GetFiles();
            foreach (System.IO.FileInfo file in files)
            {
                if (!file.Extension.Contains("ini"))
                {
                    string temppath = System.IO.Path.Combine(targetDirectory, file.Name);
                    file.CopyTo(temppath, true);
                }
            }

            // If copying subdirectories, copy them and their contents to new location.
            if (copySubDirs)
            {
                foreach (System.IO.DirectoryInfo subdir in dirs)
                {
                    string temppath = System.IO.Path.Combine(targetDirectory, subdir.Name);
                    DirectoryCopy(subdir.FullName, temppath, copySubDirs);
                }
            }
        }

        #endregion

    }
}
