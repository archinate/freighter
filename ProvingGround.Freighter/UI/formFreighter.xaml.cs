﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Forms;

using Rhino;
using Grasshopper;
using Grasshopper.Kernel.Components;
using Grasshopper.Kernel;

using System.Collections.ObjectModel;
using System.Management.Automation;
using System.Management.Automation.Runspaces;


using ProvingGround.FreighterUtils;
using ProvingGround.Freighter;



namespace ProvingGround.Freighter.UI
{
    /// <summary>
    /// Interaction logic for form_Main.xaml
    /// </summary>
    public partial class formFreighter : Window
    {
        #region private members

        RhinoDoc _doc;

        DocConfig _docConfig;
        private Grasshopper.Kernel.GH_DocumentServer _ghDocServer;
        private Grasshopper.Kernel.GH_Document _ghDoc;

        private List<string> _ghDependencies = new List<string>();
        private List<Guid> _ghDependencyGuids = new List<Guid>();

        // use an observable collection for effective WPF binding
        public ObservableCollection<DependencyPlugIns> _plugInsToCopy = new ObservableCollection<DependencyPlugIns>();
        public ObservableCollection<DependencyFiles> _filesToCopy = new ObservableCollection<DependencyFiles>();

        private string _logoPath = "";

        #endregion

        /// <summary>
        /// The main freighter building interface
        /// </summary>
        /// <param name="doc"></param>
        public formFreighter(RhinoDoc doc)
        {

            InitializeComponent();

            // get the current version
            this.Title = "Freighter v." +
                System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString() + " powered by Proving Ground";

            _doc = doc;

            // widen scope
            DoSetup();

        }

        /// <summary>
        /// Perform primary form opening processes
        /// </summary>
        private void DoSetup()
        {

            GetGrasshopper();

            if (_doc.Path == "")
            {
                label_rhinoDocument.Content = System.IO.Path.GetFileName("None");
                text_targetFolder.Text = System.IO.Path.GetDirectoryName("Get Directory...");
            }
            else
            { 
                label_rhinoDocument.Content = System.IO.Path.GetFileName(_doc.Path);
                text_targetFolder.Text = System.IO.Path.GetDirectoryName(_doc.Path);
            }
            label_ghDocument.Content = System.IO.Path.GetFileName(_ghDoc.FilePath);

            ListBox_Dependencies.ItemsSource = _plugInsToCopy;
            ListBox_Files.ItemsSource = _filesToCopy;

        }

        /// <summary>
        /// Gets the relevant data from the Grasshopper Document
        /// </summary>
        private void GetGrasshopper()
        {

            // get the currently viewed GH document
            _ghDocServer = Grasshopper.Instances.DocumentServer;
            _ghDoc = _ghDocServer[0];

            // get the directory for 3rd party GH plug-ins on this computer
            string m_appDataPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\Grasshopper\Libraries";

            //System.Reflection.Assembly assem;
            //assem.CodeBase

            // cycle through each component in the GH document
            foreach (IGH_ActiveObject m_checkComponent in _ghDoc.ActiveObjects())
            {

                if(m_checkComponent is Grasshopper.Kernel.Special.GH_Panel)
                {
                    Grasshopper.Kernel.Special.GH_Panel m_thisPanel =
                    (Grasshopper.Kernel.Special.GH_Panel)m_checkComponent;

                    if (m_thisPanel.SourceCount == 0)
                    {
                        string[] m_panelEntries = m_thisPanel.UserText.Split(new string[] { "\n" }, StringSplitOptions.RemoveEmptyEntries);
                        
                        foreach (string m_panelEntry in m_panelEntries)
                        {
                            if (System.IO.File.Exists(m_panelEntry))
                            {

                                System.IO.FileInfo m_thisInfo = new System.IO.FileInfo(m_panelEntry);

                                _filesToCopy.Add(new DependencyFiles()
                                {
                                    ComponentGuid = m_thisPanel.InstanceGuid.ToString(),
                                    ComponentNickName = m_thisPanel.NickName,
                                    ComponentType = "Panel",
                                    FileName = m_thisInfo.Name,
                                    Directory = m_thisInfo.DirectoryName,
                                    Include = true,
                                    Notes = ""
                                });
                                    
                            }
                        }
                    }

                }
                else if (m_checkComponent is Grasshopper.Kernel.Parameters.Param_FilePath)
                {
                    Grasshopper.Kernel.Parameters.Param_FilePath m_thisFilePath =
                        (Grasshopper.Kernel.Parameters.Param_FilePath)m_checkComponent;

                    if (m_thisFilePath.SourceCount == 0)
                    {
                        foreach (Grasshopper.Kernel.Types.GH_String m_fileEntry in m_thisFilePath.PersistentData.AllData(true).ToArray())
                        {
                            if (System.IO.File.Exists(m_fileEntry.ToString()))
                            {
                                System.IO.FileInfo m_thisInfo = new System.IO.FileInfo(m_fileEntry.ToString());

                                _filesToCopy.Add(new DependencyFiles()
                                {
                                    ComponentGuid = m_thisFilePath.InstanceGuid.ToString(),
                                    ComponentNickName = m_thisFilePath.NickName,
                                    ComponentType = "File Path",
                                    FileName = m_thisInfo.Name,
                                    Directory = m_thisInfo.DirectoryName,
                                    Include = true,
                                    Notes = ""
                                });
                            }
                        }
                    }
                }

                // gets the assembly associated with the component
                GH_AssemblyInfo m_thisGhAssembly = Instances.ComponentServer.FindAssemblyByObject(m_checkComponent);

                // checks that the assembly hasn't already been processed here, and that it is a 3rd party dependency
                if (!_ghDependencyGuids.Contains(m_thisGhAssembly.Id) && !m_thisGhAssembly.IsCoreLibrary)
                {

                    _ghDependencyGuids.Add(m_thisGhAssembly.Id);
                    _ghDependencies.Add(m_thisGhAssembly.Name);

                    // default note value
                    string m_assemblyNote = "";
                    // no it's not

                    // a list of all assembly files to be copied as dependencies...includes .gha and .dll files
                    var m_assemblyFiles = new List<string>();
                    var m_assemblyErrors = new List<string>();

                    // get the path for the plug in being evaluated
                    string m_plugInLocation = m_thisGhAssembly.Location;
                    m_assemblyFiles.Add(m_plugInLocation);

                    string m_plugInDirectory = System.IO.Path.GetDirectoryName(m_plugInLocation);

                    // default of whether or not to include the plug-in by default:
                    // determined by whether or not the plug-in is in the app data folder
                    bool m_standardPlugIn = false;
                    string m_newDirectory = "";

                    if (m_plugInDirectory == m_appDataPath) // this plug-in is loaded in the standard GH library
                    {
                        m_standardPlugIn = true;
                    }
                    else if (m_plugInLocation.Contains(m_appDataPath)) // this plug-in is loaded into a sub-directory within the standard GH library
                    {
                        m_standardPlugIn = true;
                        m_newDirectory = m_plugInDirectory.Split(System.IO.Path.DirectorySeparatorChar).Last();
                    }
                    else
                    {
                        m_newDirectory = m_plugInDirectory.Split(System.IO.Path.DirectorySeparatorChar).Last(); // a non-standard plug-in...warnings abound
                    }


                    //// OLD METHOD
                    //// get all dll's in assembly directory to compare against its dependencies
                    //var m_dlls = System.IO.Directory.GetFiles(m_plugInDirectory, "*.dll");
                    //var m_dllAssemblyNames = new List<string>();

                    //foreach (string m_dll in m_dlls)
                    //{
                    //    try
                    //    {
                    //        m_dllAssemblyNames.Add(System.Reflection.AssemblyName.GetAssemblyName(m_dll).Name.ToUpper());
                    //    }
                    //    catch { }
                    //}

                    //// get local assembly dependencies (checks them against those that exist in the .gha source directory)
                    //var m_assemblyNames = m_thisGhAssembly.Assembly.GetReferencedAssemblies();

                    //foreach (System.Reflection.AssemblyName m_thisAssemblyName in m_assemblyNames)
                    //{
                        
                    //    if (m_dllAssemblyNames.Contains(m_thisAssemblyName.Name.ToUpper()))
                    //    {
                    //        string m_dllSource = m_dlls[m_dllAssemblyNames.IndexOf(m_thisAssemblyName.Name.ToUpper())];
                    //        m_assemblyFiles.Add(m_dlls[m_dllAssemblyNames.IndexOf(m_thisAssemblyName.Name.ToUpper())]);
                    //    }
                    //}

                    AddAssembly(m_thisGhAssembly.Assembly, System.IO.Path.GetDirectoryName(m_thisGhAssembly.Location), ref m_assemblyFiles, ref m_assemblyErrors);

                    bool m_hasErrors = (m_assemblyErrors.Count > 0);

                    // adds text related to external plug-ins as a default to include in the readme.txt file that will accompany the launcher
                    if (!m_standardPlugIn)
                    {
                        m_assemblyNote += m_thisGhAssembly.Name + " appears to be an external plug-in, and should be installed using the appropriate installer. Its directory on the computer authoring this packager is " 
                            + m_plugInDirectory + (m_hasErrors ? "\n" : "");
                    }

                    if (m_hasErrors)
                    {

                        m_assemblyNote += "Freighter was unable to find one or more dependencies associated with this plug-in. It is recommended that the end user install this plug-in independently of Freighter to ensure that all necessary resources are available." + "\n" + "Error list:";

                        foreach (string m_error in m_assemblyErrors)
	                    {
                            m_assemblyNote += "\n" + m_error;
	                    }
                    }

                    // create binding for the list of dependencies
                    _plugInsToCopy.Add(new DependencyPlugIns() { Name = m_thisGhAssembly.Name, Include = m_standardPlugIn, Type = m_standardPlugIn ? "Standard" : "External", Resources = m_assemblyFiles, Errors = m_assemblyErrors, HasErrors = m_hasErrors, NewDirectory = m_newDirectory, Path = m_plugInDirectory, Notes = m_assemblyNote });

                }

            }

        }

        /// <summary>
        /// Add assemblies
        /// </summary>
        /// <param name="assemblyCheck"></param>
        /// <param name="location"></param>
        /// <param name="fileLookup"></param>
        /// <param name="errors"></param>
        /// <returns></returns>
        public bool AddAssembly(System.Reflection.Assembly assemblyCheck, string location, ref List<string> fileLookup, ref List<string> errors)
        {

            UriBuilder m_baseUri = new UriBuilder(assemblyCheck.CodeBase);
            string m_basePath = Uri.UnescapeDataString(m_baseUri.Path);

            System.Reflection.AssemblyName[] m_referencedAssemblies = assemblyCheck.GetReferencedAssemblies();

            foreach (System.Reflection.AssemblyName m_thisReferencedAssembly in m_referencedAssemblies)
            {
                try
                {
                    System.Reflection.Assembly m_assembly = System.Reflection.Assembly.Load(m_thisReferencedAssembly);
                    UriBuilder m_uri = new UriBuilder(m_assembly.CodeBase);
                    string m_path = Uri.UnescapeDataString(m_uri.Path);

                    if (System.IO.Path.GetDirectoryName(m_path) == location)
                    {
                        if (!fileLookup.Contains(m_path))
                        {
                            fileLookup.Add(m_path);
                        }
                        AddAssembly(m_assembly, System.IO.Path.GetDirectoryName(m_path), ref fileLookup, ref errors);
                    }
                }
                catch (Exception ex)
                {
                    errors.Add(ex.Message);
                }
            }

            return false;
        }

        /// <summary>
        /// Create a package from all of the collected data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button_Package_Click(object sender, RoutedEventArgs e)
        {
            // Name for the new package
            string m_newPackageName = text_packageName.Text;

            // tests that the package name exists and is valid
            if (m_newPackageName == "" || m_newPackageName.IndexOfAny(System.IO.Path.GetInvalidFileNameChars()) >= 0)
            {
                System.Windows.Forms.MessageBox.Show("Check that the new package name is valid.", "Freighter");
                return;
            }

            // tests that the target directory exists and is valid
            if (!System.IO.Directory.Exists(text_targetFolder.Text) || text_targetFolder.Text.IndexOfAny(System.IO.Path.GetInvalidPathChars()) >= 0)
            {
                System.Windows.Forms.MessageBox.Show("Check that the target directory exists and/or has a valid name.", "Freighter");
                return;
            }

            // if the package already exists in the target directory, determines if the user wants to overwrite it
            if (System.IO.Directory.Exists(text_targetFolder.Text + "\\" + m_newPackageName + ""))
            {
                System.Windows.Forms.DialogResult m_directoryOverwrite =
                    System.Windows.Forms.MessageBox.Show("Overwrite the existing " + m_newPackageName + "?", "Freighter", MessageBoxButtons.YesNo);

                if (m_directoryOverwrite == System.Windows.Forms.DialogResult.Yes)
                {
                    System.IO.Directory.Delete(text_targetFolder.Text + "\\" + m_newPackageName + "", true);
                }
                else
                {
                    return;
                }
            }

            // creates the base directories
            System.IO.Directory.CreateDirectory(text_targetFolder.Text + "\\" + m_newPackageName + "");
            System.IO.Directory.CreateDirectory(text_targetFolder.Text + "\\" + m_newPackageName + "\\Files");
            System.IO.Directory.CreateDirectory(text_targetFolder.Text + "\\" + m_newPackageName + "\\PlugIns");
            System.IO.Directory.CreateDirectory(text_targetFolder.Text + "\\" + m_newPackageName + "\\Attachments");
            System.IO.Directory.CreateDirectory(text_targetFolder.Text + "\\" + m_newPackageName + "\\Resources");

            _docConfig = new DocConfig() {Attributes = new List<DocAttribute>(), Toggles = new List<ToggleEntry>(), AttachmentComps = new List<AttachmentComponent>()};

            try
            {
                // copy essential resources to the output folders
                System.IO.File.WriteAllBytes(text_targetFolder.Text + "\\" + m_newPackageName + "\\" + m_newPackageName + ".exe", Freighter.Resources.ProvingGround_FreightLauncher);
                System.IO.File.WriteAllBytes(text_targetFolder.Text + "\\" + m_newPackageName + "\\ProvingGround.FreighterUtils.dll", Freighter.Resources.ProvingGround_FreighterUtils);

                // create and write the readme.txt file
                var m_readMeText = new List<string>();
                m_readMeText.Add(m_newPackageName);
                m_readMeText.Add(DateTime.Now.ToString("dd-MMM-yyyy"));
                m_readMeText.Add("Freighter");

                string m_underscore = "";
                for (int i = 0; i < Math.Max(m_readMeText[0].Length, m_readMeText[2].Length); i++) m_underscore += "-";
                m_readMeText.Add(m_underscore);

                m_readMeText.Add(" ");

                var m_notes = new List<string>();

                foreach (var m_thisPlugIn in _plugInsToCopy)
                {
                    if (m_thisPlugIn.Notes != "")
                    {
                        m_notes.Add(m_thisPlugIn.Notes);
                        m_notes.Add("  ");
                    }

                    if (m_thisPlugIn.Include)
                    {
                        m_readMeText.Add("Main dependencies for " + m_thisPlugIn.Name + ":");
                        foreach (string m_thisResource in m_thisPlugIn.Resources)
                        {
                            m_readMeText.Add("  " + System.IO.Path.GetFileName(m_thisResource));
                        }
                        if (m_thisPlugIn.NewDirectory != "") m_readMeText.Add("*This plug-in will be located in its own directory: " + m_thisPlugIn.NewDirectory + ", with the above and potentially additional components added.");
                        m_readMeText.Add("  ");
                    }
                }

                if (m_notes.Count > 0)
                {
                    m_readMeText.Add(m_underscore);
                    m_readMeText.Add("  ");
                    m_readMeText.AddRange(m_notes);
                }

                System.IO.File.WriteAllLines(text_targetFolder.Text + "\\" + m_newPackageName + "\\readme.txt", m_readMeText);

                // copies the rhino document and grasshopper definition to the "files" directory
                if (_doc.Path != "") System.IO.File.Copy(_doc.Path, text_targetFolder.Text + "\\" + m_newPackageName + "\\Files\\" + System.IO.Path.GetFileName(_doc.Path));
                _docConfig.Attributes.Add(new DocAttribute() { Attribute = "RhinoFileName", Value = label_rhinoDocument.Content.ToString() });
                
                
                System.IO.File.Copy(_ghDoc.FilePath, text_targetFolder.Text + "\\" + m_newPackageName + "\\Files\\" + System.IO.Path.GetFileName(_ghDoc.FilePath));
                _docConfig.Attributes.Add(new DocAttribute() { Attribute = "GrasshopperFileName", Value = System.IO.Path.GetFileName(_ghDoc.FilePath) });

                // cycles through all of the dependencies and copies them to associated sub-directories in the "dependencies" directory
                foreach (DependencyPlugIns m_thisPlugIn in _plugInsToCopy)
                {
                    if (m_thisPlugIn.Include)
                    {
                        if (m_thisPlugIn.NewDirectory == "")
                        {
                            foreach (string m_thisResource in m_thisPlugIn.Resources)
                            {
                                try
                                {
                                    System.IO.File.Copy(m_thisResource, text_targetFolder.Text + "\\" + m_newPackageName + "\\PlugIns\\" + System.IO.Path.GetFileName(m_thisResource));
                                }
                                catch (Exception) { }
                            }
                        }
                        else
                        {
                            try
                            {
                                DirectoryCopy(m_thisPlugIn.Path, text_targetFolder.Text + "\\" + m_newPackageName + "\\PlugIns\\" + m_thisPlugIn.NewDirectory, true);
                            }
                            catch (Exception) { }
                        }

                    }

                }

                string m_logoName = "";

                if (System.IO.File.Exists(_logoPath))
                {
                    m_logoName = new System.IO.FileInfo(_logoPath).Name;
                    System.IO.File.Copy(_logoPath, text_targetFolder.Text + "\\" + m_newPackageName + "\\Resources\\" + m_logoName);                    
                }

                _docConfig.Attributes.Add(new DocAttribute() { Attribute = "LogoPath", Value = m_logoName });

                // create the Application config file
                Dictionary<string, List<string>> m_fileCopyDictionary = new Dictionary<string, List<string>>();

                foreach (DependencyFiles m_fileToCopy in _filesToCopy)
                {
                    if (m_fileToCopy.Include)
                    {

                        if (!m_fileCopyDictionary.Keys.Contains(m_fileToCopy.ComponentGuid))
                        {
                            m_fileCopyDictionary.Add(m_fileToCopy.ComponentGuid, new List<string>());
                        }

                        m_fileCopyDictionary[m_fileToCopy.ComponentGuid].Add(m_fileToCopy.FileName);

                        if (!System.IO.File.Exists(text_targetFolder.Text + "\\" + m_newPackageName + "\\Attachments\\" + m_fileToCopy.FileName))
                        {
                            System.IO.File.Copy(m_fileToCopy.Directory + "\\" + m_fileToCopy.FileName,
                            text_targetFolder.Text + "\\" + m_newPackageName + "\\Attachments\\" + m_fileToCopy.FileName);
                        }
                        
                    }
                }

                foreach (string m_component in m_fileCopyDictionary.Keys)
                {
                    Guid m_componentInstance = Guid.Parse(m_component);
                    _docConfig.AttachmentComps.Add(new AttachmentComponent() { InstanceGuid = m_componentInstance, FileNames = m_fileCopyDictionary[m_component] });
                }

                // Toggles for showing/hiding Grasshopper on opening
                _docConfig.Toggles.Add(new ToggleEntry() { Toggle = "ShowGrasshopperRCP", Value = (bool)CheckBox_ShowRCP.IsChecked });
                _docConfig.Toggles.Add(new ToggleEntry() { Toggle = "HideGrasshopper", Value = (bool)CheckBox_HideGH.IsChecked });

                DocConfig.WriteConfig(text_targetFolder.Text + "\\" + m_newPackageName + "\\freight.xml", _docConfig);

                //_docConfig.
                //// fix here
                //_docConfig = new DocConfig((bool)CheckBox_HideGH.IsChecked, (bool)CheckBox_ShowRCP.IsChecked, 
                //    m_logoName, m_fileCopyDictionary);
                //_docConfig.writeConfig(text_targetFolder.Text +"\\" + m_newPackageName + "\\config.xml");

                System.Windows.Forms.MessageBox.Show("Grasshopper executable created!", "Proving Ground");

                // closes the form
                this.Close();
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show("Failed to create new Freight: " + ex.Message);
            }
        }

        

        #region Form Operations

        /// <summary>
        /// Cancels the operation; close the form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button_Cancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Finds a directory
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button_Browse_Click(object sender, RoutedEventArgs e)
        {
            FolderBrowserDialog m_directoryDialog = new FolderBrowserDialog();
            DialogResult result = m_directoryDialog.ShowDialog();

            text_targetFolder.Text = m_directoryDialog.SelectedPath;

        }

        /// <summary>
        /// Opens the Add/Edit form for the associated dependency
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button_AddEdit_Click(object sender, RoutedEventArgs e)
        {
            // gets the index of the embedded button in the datagrid in the WPF form
            // !!! VERY USEFUL !!!
            var m_item = (sender as FrameworkElement).DataContext;
            int m_index = ListBox_Dependencies.Items.IndexOf(m_item);

            UI.formAddEditNote Edit = new formAddEditNote(_plugInsToCopy[m_index].Notes, m_index, this);
            Edit.Show();
        }

        /// <summary>
        /// Warns the user if an item checked for inclusion is an external directory
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void chkInclude_Checked(object sender, RoutedEventArgs e)
        {
            var m_item = (sender as FrameworkElement).DataContext;
            int m_index = ListBox_Dependencies.Items.IndexOf(m_item);

            if (_plugInsToCopy[m_index].Type == "External")
            {
                System.Windows.Forms.DialogResult m_really =
                    System.Windows.Forms.MessageBox.Show("WARNING! Including this external plug-in will load it to a directory it doesn't seem intended for. It will only work if it hasn't already been installed on the user's computer, and will break if they try to install it through proper channels later. Do you really want to do this?", "Proving Ground", MessageBoxButtons.YesNo);
                if (m_really == System.Windows.Forms.DialogResult.No)
                {
                    _plugInsToCopy[m_index].Include = false;
                    (sender as System.Windows.Controls.CheckBox).IsChecked = false;
                }
            }
        }

        /// <summary>
        /// Adds/edits logos
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_AddEditLogo_Click(object sender, RoutedEventArgs e)
        {

            // Create an instance of the open file dialog box.
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            // Set filter options and filter index.
            openFileDialog1.Filter = "Image Files (*png, *.bmp, *.jpg)|*png;*.bmp;*.jpg";
            openFileDialog1.FilterIndex = 1;

            // Process input if the user clicked OK.
            if(openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                // Open the selected file to read.
                if (System.IO.File.Exists(openFileDialog1.FileName))
                { 
                    _logoPath = openFileDialog1.FileName;
                    Button_AddEditLogo.Content = "Edit Logo...";
                }
            }

        }

        #endregion

        #region File Operations
        /// <summary>
        /// Copy the contents of one directory into another (from MSDN)
        /// </summary>
        /// <param name="sourceDirName"></param>
        /// <param name="destDirName"></param>
        /// <param name="copySubDirs"></param>
        private void DirectoryCopy(string sourceDirName, string destDirName, bool copySubDirs)
        {
            // Get the subdirectories for the specified directory.
            System.IO.DirectoryInfo dir = new System.IO.DirectoryInfo(sourceDirName);

            if (!dir.Exists)
            {
                throw new System.IO.DirectoryNotFoundException(
                    "Source directory does not exist or could not be found: "
                    + sourceDirName);
            }

            System.IO.DirectoryInfo[] dirs = dir.GetDirectories();
            // If the destination directory doesn't exist, create it.
            if (!System.IO.Directory.Exists(destDirName))
            {
                System.IO.Directory.CreateDirectory(destDirName);
            }

            // Get the files in the directory and copy them to the new location.
            System.IO.FileInfo[] files = dir.GetFiles();
            foreach (System.IO.FileInfo file in files)
            {
                string temppath = System.IO.Path.Combine(destDirName, file.Name);
                file.CopyTo(temppath, false);
            }

            // If copying subdirectories, copy them and their contents to new location.
            if (copySubDirs)
            {
                foreach (System.IO.DirectoryInfo subdir in dirs)
                {
                    string temppath = System.IO.Path.Combine(destDirName, subdir.Name);
                    DirectoryCopy(subdir.FullName, temppath, copySubDirs);
                }
            }
        }

        #endregion

    }

    public class DependencyPlugIns
    {

        public string Name { get; set; }
        public string Type { get; set; }
        public bool Include { get; set; }
        public string Path { get; set; }
        public string NewDirectory { get; set; }
        public List<string> Resources { get; set; }
        public List<string> Errors { get; set; }
        public bool HasErrors { get; set; }
        public string Notes { get; set; }

    }

    //public class PlugIn
    //{

    //    [System.Xml.Serialization.XmlElementAttribute("PlugInName")]
    //    public string PlugInName { get; set; }

    //    [System.Xml.Serialization.XmlElementAttribute("IsStandard")]
    //    public bool IsStandard { get; set; }

    //    [System.Xml.Serialization.XmlElementAttribute("Include")]
    //    public bool Include { get; set; }

    //    [System.Xml.Serialization.XmlElementAttribute("SourceDirectory")]
    //    public string SourceDirectory { get; set; }

    //    [System.Xml.Serialization.XmlElementAttribute("TargetDirectory")]
    //    public string TargetDirectory { get; set; }

    //    [System.Xml.Serialization.XmlArrayAttribute("Resources")]
    //    [System.Xml.Serialization.XmlArrayItemAttribute("Resource")]
    //    public List<string> Resources { get; set; }

    //    [System.Xml.Serialization.XmlElementAttribute("Note")]
    //    public string Note { get; set; }

    //    public PlugIn() { }

    //}

    public class DependencyFiles
    {

        public string FileName { get; set; }
        public string Directory { get; set; }
        public bool Include { get; set; }
        public string ComponentType { get; set; }
        public string ComponentGuid { get; set; }
        public string ComponentNickName { get; set; }
        public string Notes { get; set; }

    }
}
