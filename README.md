# Development for Freighter has been discontinued... #
Greetings Freighter users,

Please note that I have decided to stop development and maintenance on this project. Feel free to use and 
reference this code if you find it valuable in your work. If you are interested in tools to help you manage and distribute
your Grasshopper definitions, please check out [Hypar](http://hypar.io) or [ShapeDiver.](http://shapediver.com)

Thank you for your interest - I hope the code can provide some value even though I will no longer be working 
on this project.

Best regards,

Nate Miller

CEO, [Proving Ground](http://provingground.io)

![FreighterLogoSmall.png](https://bitbucket.org/repo/qrxjae/images/1902729188-FreighterLogoSmall.png)

### What is Freighter? ###


Freighter is an open source plug-in authored by [Proving Ground.](http://ProvingGround.io) Freighter makes it easy to share your Grasshopper computational design tools with other users. Freighter provides utilities for the automatic packaging and launching of Grasshopper definitions, Rhino files, and other important dependencies. 

### Who maintains Freighter? ###

* Freighter is authored and maintained by [Proving Ground's own Dave Stasiuk](http://provingground.io/about/dave-stasiuk/)
* If you have questions, visit our [Freighter Website](http://provingground.io/tools/freighter-for-grasshopper/)

### How do I get started? ###

* Current "official" Freighter builds can be installed through a [Click Once installer link.](http://provingground-freighter.s3-website-us-east-1.amazonaws.com/ProvingGround.Freighter.Installer.application)
* You can also build the Freighter project from the source code (Visual Studio 2013 C# Project)

Requirements:

* [Rhinoceros 5.0 (64-Bit)](https://www.rhino3d.com/)
* [Grasshopper](http://www.grasshopper3d.com/)

### Where can I learn how to use Freighter? ###
[Visit our Bitbucket wiki](https://bitbucket.org/archinate/freighter/wiki/Home) for help getting started.

### How can I contribute? ###

If you have come across bugs or have wish list items, [submit them to the Issues section of the Freighter repo.](https://bitbucket.org/archinate/freighter/issues?status=new&status=open)

If you have built some cool stuff for Freighter and would like to share it back with the official project, you can follow these steps...

*  Fork the Freighter repository
*  Make cool stuff.
*  Submit a Pull request.

### Thanks! ###
We'd like to say thanks to....

* [HDR's](http://www.hdrinc.com) Practice Innovation team.  In particular, we'd like to thank Matt Goldsberry and Joel Yow for being early adopters and testing the tool.
* [Robert McNeel & Associates](http://www.en.na.mcneel.com/) for providing great tools that can be customized and extended.

### The MIT License (MIT) ###

Freighter is an open source project under the [MIT](http://opensource.org/licenses/MIT) license.  Freighter is experimental software and may contain bugs and incomplete features.

Copyright (c) 2016 Proving Ground LLC

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.